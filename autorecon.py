#!/usr/bin/python
# -* coding: utf-8 *-

import requests
import socket
import re
import os
import os.path
import argparse
import random
import threading

parser = argparse.ArgumentParser()
parser.add_argument('-iL', '--hosts-list', type=str, help='List of hosts to scan')
parser.add_argument('--max-threads', type=int, default=3, help='Maximum number of threads')
parser.add_argument('--user-files', type=str, default='/usr/share/seclists/Usernames/cirt-default-usernames.txt')
args = parser.parse_args()
pool = threading.BoundedSemaphore(value=args.max_threads)
fullScanPool = threading.BoundedSemaphore(value=2)
print(args)

try:
	host_list = map(str.strip,open(args.hosts_list).readlines())
except IOError:
	host_list = []

def quick_scan(host):
	print('Initiating quick scan for {0}'.format(host))
	status = os.system('nmap --open -oG {host}/{host}_quick.gnmap -oN {host}/{host}_quick.nmap {host}'.format(host=host))
	print('Quick scan for {0} completed'.format(host))
	return status

def full_scan(host):
	print('Initiating full scan for {0}'.format(host))
	status = os.system('nmap -p1-65535 --open -oG {host}/{host}_full.gnmap -oN {host}/{host}_full.nmap {host}'.format(host=host))
	print('Full scan for {0} completed'.format(host))
	return status

def create_host_folder(host):
	if not os.path.exists(host):
		print('Creating folder for {0}'.format(host))
		os.makedirs(host)

def get_open_ports(host, scanType = 'quick'):
	print('Retrieving open ports for {0}'.format(host))
	f = None
	portsList = []
	if scanType == 'quick':
		f = open('{host}/{host}_quick.gnmap'.format(host=host))
	elif scanType == 'full':
		f = open('{host}/{host}_full.gnmap'.format(host=host))
	else:
		print('{host}/{host}_full.gnmap does not exist'.format(host=host))
	if f:
		for line in f:
			if 'Ports: ' in line:
				portsList = re.findall(r'(\d+)/open',line)
				break
		f.close()
	return map(int, portsList)

def enumerate_ftp(host):
	print('Enumerating FTP service for {0}'.format(host))
	os.system('nmap -p21 -sV --script=ftp-anon -oG ftp_anonymous_login.gnmap -oN ftp_anonymous_login.nmap {0}'.format(host))
	print('Done enumerating FTP service for {0}'.format(host))

class SMTPClient:
def __init__(self, host):
	self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	self.host = host
	self.users = []

	def connect(self)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Connect to the Server
		connect=self.sock.connect((args.host,25))
		# Receive the banner
		banner=self.sock.recv(1024)
		#print banner
	
	def close(self):
		self.sock.close()

	def vrfy(self, user):
		#print('[+] Checking user {0}'.format(user))
		self.sock.send('VRFY {0}\r\n'.format(user.rstrip()))
		result=self.sock.recv(1024)
		#print result
		if '252' in result:
			self.users.append(user)

def enumerate_smtp(host):
	print('Enumerating SMTP service for {0}'.format(host))
	client = SMTPClient(host)


	files = args.users_files.split(',')
	client.connect()
	for userFile in files:
		if os.path.isfile(userFile):
			with open(userFile) as f:
				for user in f:
					try:
						vrfy(user)
					except socket.error:
						#print('[-] Reconnecting to server')
						client.connect()
						client.vrfy(user)
	client.close()

	if client.users != []:
		with open('{0}/smtp_user_enum.txt','w') as f:
			f.write(client.users[0])
			for user in client.users[1:]:
				f.write('\n{0}'.format(user))
	
	print('Done enumerating SMTP service for {0}'.format(host))

def enumerate_dns(host):
	print('Enumerating DNS service for {0}'.format(host))
	if re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',host):
		os.system('dig -x {0}|tee {0}/dns_lookup.txt'.format(host))
	else:
		os.system('dig {0}|tee/dns_lookup.txt'.format(host))
	#TODO: add DNS bruteforce, zonetransfer, maybe reverse DNS bruteforce...
	print('Done numerating DNS service for {0}'.format(host))

def crawl(host, scheme='http', path='/', visited = []):
	if path not in visited:
		visited.append(path)
	else:
		return visited
	print('HTTP enumeration for {0}: Crawling {1}'.format(host, path))
	r = requests.get(url=scheme+'://'+host+path, verify=False)

	#Find links in page
	html = r.text
	links = re.findall(r'href="([^"]+)"', html)

	#Remove anchors and external links
	tmp = []
	for link in links:
		if link in tmp:
			continue
		if link[0] == '/':
			tmp.append(link)
		elif link[0] != '#':
			m = re.match(r'^https?://([^/]+)(.*)',link)
			if m:
				if m.group(1) == host:
					if m.group(2) != '' and m.group(2) not in tmp:
						tmp.append(m.group(2))
			else:
				if '://' not in link and path + '/' + link not in tmp:
					tmp.append(path + '/' + link)
	
	links = []
	for link in tmp:
		if link in visited:
			pass
		else:
			links.append(link)
	
	print('HTTP enumeration for {0}: Next links to visit: {1}'.format(host, links))
	for link in links:
		visited = crawl(host=host, scheme=scheme, path=link, visited=visited)
	return visited

def gobuster(host, scheme='http', path='/'):
	gobusterOutput = '{0}/gobuster_scan_{1}.txt'.format(host,path.replace('/','%2F'))

	os.system('gobuster -q -r -s "200,202,204,301,302,304,307,401,403,500" -o {2} -u {0}://{1}{3} -w /usr/share/seclists/Discovery/Web-Content/common.txt'.format(scheme,host,gobusterOutput,path))

	pages = []
	with open(gobusterOutput) as go:
		#Crawl gobuster 20X URLs
		for line in go:
			m = re.match(r'^(.+)\s\(Status: (\d{3})\)', line)
			if m:
				page = m.group(1)
				status = m.group(2)
				if 200 <= int(status) < 300:
					newPages = crawl(host=host,scheme=scheme, path=page, visited=pages)
					for newPage in newPages:
						if newPage not in pages:
							pages.append(newPage)
	return pages
						
			

def enumerate_http(host, port):
	print('Enumerating HTTP(s) service for {0}'.format(host))
	protocol = 'http'
	if port == 443:
		protocol = 'https'
	r = requests.get(url='{0}://{1}'.format(protocol, host), verify=False)

	#Get HTTP headers
	serverHeaders = r.headers
	print('HTTP server headers for {0}: {1}'.format(host,serverHeaders))

	#Crawl website
	pages = crawl(host=host,scheme=protocol)
	print("Pages found:")
	for page in pages:
		print(page)
	
	#Check robots.txt
	r = requests.get('{0}://{1}/robots.txt'.format(protocol, host))
	robots = None
	if r.status_code >= 200 and r.status_code < 300:
		robots = r.text
	if robots:
		print('robots.txt found:')
		print(robots)
		open('{0}/robots.txt'.format(host),'w').write(robots)

	#Do nikto scan
	print('Starting Nikto scan on host {0}'.format(host))
	os.system('echo n|nikto -h {0}://{1} -output {1}/nikto_scan.txt'.format(protocol,host))
	print('Nikto scan done for host {0}'.format(host))

	#Do gobuster scan
	pages = gobuster(host=host, scheme=protocol)
	print("Pages found (updated after gobuster scan):")
	for page in pages:
		print(page)
		
def enumerate_smb(host,domain='WORKGROUP',user='',password=''):
	print('Enumerating SMB service for {0}'.format(host))
	#Tools: nmap, smbclient, smbmap, enum4linux, nbtscan
	os.system('nbtscan {0} > {0}/nbtscan.txt'.format(host))

	print('Starting nmap scan on {0} for SMB'.format(host))
	os.system('nmap -sV -p 139,445 -oN {host}/smb_service_discovery.nmap -oG smb_service_discovery.gnmap {host}'.format(host=host))
	os.system('nmap --script=smb-vuln-* --script-args=unsafe -p 139,445 -oN {host}/smb_vuln_discovery.nmap -oG smb_vuln_discovery.gnmap {host}'.format(host=host))
	print('nmap scan for SMB on {0} done'.format(host))

	print("Starting smbmap scan on {0}".format(host))
	os.system('smbmap -H {host} -R -d \'{domain}\' -u \'{user}\' -p \'{password}\' > {host}/smbmap.txt'.format(host=host, domain=domain, user=user, password=password))
	print("smbmap scan on {0} done".format(host))

	#smbclient -N -L \\\\{host}
	print('Listing shares on {0} with smbclient'.format(host))
	os.system('smbclient -N -L \\\\{host} > {host}/smbclient.txt'.format(host=host))
	print('Done listing shares on {0} with smbclient'.format(host))

	#enum4linux -a -u {username} -p {password} {host}
	print('Starting enum4linux on {0}'.format(host))
	os.system('enum4linux -a -u \'{username}\' -p \'{password}\' {host} > {host}/enum4linux.txt'.format(username=user, password=password,host=host))
	print('enum4linux completed on {0}'.format(host))
	print('Done enumerating SMB service for {0}'.format(host))

def enumerate_ldap(host):
	print('Enumerating LDAP service for {0}'.format(host))
	print('Done enumerating LDAP service for {0}'.format(host))


class Enumerator(threading.Thread):
	def __init__(self, host, threadPool):
		threading.Thread.__init__(self)
		self.host = host
		self.threadPool = threadPool

	def run(self):
		self.threadPool.acquire()

		if self.threadPool == fullScanPool:
			scanType = 'full'
			print('*Enumerator for {0} acquired lock on fullScanPool'.format(self.host))
		else:
			scanType = 'quick'
			print('Enumerator for {0} acquired lock on pool'.format(self.host))

		portsList = get_open_ports(self.host, scanType)

		for port in portsList:
			if port == 21:
				enumerate_ftp(self.host)
			elif port == 25:
				enumerate_smtp(self.host)
			elif port == 53:
				enumerate_dns(self.host)
			elif port in (80,443):
				enumerate_http(self.host,port)
			elif port == 389:
				enumerate_ldap(self.host)
			elif port == 445:
				enumerate_smb(self.host)
			else:
				pass

		if self.threadPool == fullScanPool:
			print('*Enumerator for {0} releasing lock on fullScanPool'.format(self.host))
		else:
			print('Enumerator for {0} releasing lock on pool'.format(self.host))

		self.threadPool.release()
	
class Scanner(threading.Thread):
	def __init__(self, host, target=quick_scan):
		threading.Thread.__init__(self)
		self.host = host
		self.target = target

	def run(self):
		if self.target == quick_scan:
			pool.acquire()
			print("Scanner for {0} acquired lock on pool".format(self.host))
		else:
			fullScanPool.acquire()
			print("Scanner for {0} acquired lock on fullScanPool".format(self.host))
		self.target(self.host)
		if self.target == quick_scan:
			print("Scanner for {0} releasing lock on pool".format(self.host))
			pool.release()
			Enumerator(self.host, pool).start()
		else:
			print("Scanner for {0} releasing lock on fullScanPool".format(self.host))
			fullScanPool.release()
			Enumerator(self.host, fullScanPool).start()

if __name__ == '__main__':
	#Create the necessary folders
	for host in host_list:
		create_host_folder(host)
	
	#Quickly scan first host and enumerate the services found
	firstTargetWorker = Scanner(host_list[0], quick_scan)
	firstTargetWorker.start()

	#Perform full scan on the other hosts
	for host in host_list[1:]:
		Scanner(host, full_scan).start()

	#Wait for initial enumeration to complete
	firstTargetWorker.join()

	#Perform a full scan on the first host
	full_scan(host_list[0])

	#Enumerate any service not yet found
	quickPortsList = get_open_ports(host_list[0],'quick')
	portsList = get_open_ports(host_list[0], 'full')

	for port in portsList:
		if port not in quickPortsList:
			if port == 21:
				enumerate_ftp(host_list[0])
			elif port == 25:
				enumerate_smtp(host_list[0])
			elif port == 53:
				enumerate_dns(host_list[0])
			elif port in (80,443):
				enumerate_http(host_list[0],port)
			elif port == 389:
				enumerate_ldap(host_list[0])
			elif port == 445:
				enumerate_smb(host_list[0])
			else:
				pass
